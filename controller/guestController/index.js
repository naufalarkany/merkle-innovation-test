const repository = require('../../repositories/guests.repository');

exports.create = async (req, res) => {
    try {
        const { name, phoneNumber } = req.body;

        //validation required body
        if (!name || !phoneNumber) {
            throw new Error('fill empty field');
        }
        const createdGuest = await repository.create(req.body);
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully added',
            data: createdGuest,
        });
    } catch (error) {
        console.log(error);
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};

exports.getAll = async (req, res) => {
    try {
        const guests = await repository.getAll();
        return res.status(200).send({
            status: 'success',
            msg: 'Get All Guest Success',
            data: guests,
        });
    } catch (error) {
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};

exports.getAllForGuests = async (req, res) => {
    try {
        const guests = await repository.getAllForGuests();
        return res.status(200).send({
            status: 'success',
            msg: 'Get All Guest Success',
            data: guests,
        });
    } catch (error) {
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};

exports.getOneById = async (req, res) => {
    try {
        const { guestId } = req.params;
        const guest = await repository.getOne(guestId);
        return res.status(200).send({
            status: 'success',
            msg: 'Get Guest Success',
            data: guest ? guest : {},
        });
    } catch (error) {
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};

exports.update = async (req, res) => {
    try {
        const { guestId } = req.params;
        const guest = await repository.getOne(guestId);
        if (!guest) {
            throw new Error('Guest not exist');
        }
        await repository.edit(guestId, req.body);
        return res.status(200).send({
            status: 'success',
            msg: 'Get Guest Success',
        });
    } catch (error) {
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};

exports.delete = async (req, res) => {
    try {
        const { guestId } = req.params;
        const guest = await repository.getOne(guestId);
        if (!guest) {
            throw new Error('Guest not exist');
        }
        await repository.delete(guestId);
        return res.status(200).send({
            status: 'success',
            msg: 'Delete Guest Success',
        });
    } catch (error) {
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};
