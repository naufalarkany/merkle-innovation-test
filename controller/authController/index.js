const repository = require('../../repositories/user.repository');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.create = async (req, res) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) {
            throw new Error('fill empty field');
        }
        const user = await repository.findByUsername(username);
        if (user) {
            throw new Error('username already registered');
        }
        const createdUser = await repository.create(req.body);
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully created new User Admin',
            data: createdUser,
        });
    } catch (error) {
        console.log(error);
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};

exports.login = async (req, res) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) {
            throw new Error('fill empty field');
        }
        const user = await repository.findByUsername(username);
        if (!user || !bcrypt.compareSync(password, user.password)) {
            throw new Error('Please check again your username and password');
        }
        const accessToken = jwt.sign(
            {
                id: user.id,
                username: user.username,
            },
            process.env.ACCESS_TOKEN_SECRET,
            {
                expiresIn: '1y',
            }
        );
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully login as Admin',
            data: {
                accessToken,
                username: user.username,
            },
        });
    } catch (error) {
        console.log(error);
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
};
