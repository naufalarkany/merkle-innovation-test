const guest = require('./guestController');
const auth = require('./authController');

module.exports = {
    guest,
    auth,
};
