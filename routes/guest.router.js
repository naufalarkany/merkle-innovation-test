const { Router } = require('express');
const { guest } = require('../controller');
const router = Router();

router.post('/', guest.create);
router.get('/', guest.getAllForGuests);
module.exports = router;
