const { Router } = require('express');
const { auth, guest } = require('../controller');
const authenticationMiddleware = require('../middleware/auth');
const router = Router();

router.post('/register', auth.create);
router.post('/login', auth.login);
router.get('/guest', authenticationMiddleware, guest.getAll);
router.get('/guest/:guestId', authenticationMiddleware, guest.getOneById);
router.put('/guest/:guestId', authenticationMiddleware, guest.update);
router.delete('/guest/:guestId', authenticationMiddleware, guest.delete);

module.exports = router;
