const { Router } = require('express');
const guestRouter = require('./guest.router');
const adminRouter = require('./admin.router');
const router = Router();

// Routes for guests interaction
router.use('/guests', guestRouter);

// Routes for admins interaction
router.use('/admins', adminRouter);
module.exports = router;
