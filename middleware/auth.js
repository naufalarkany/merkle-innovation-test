const jwt = require('jsonwebtoken');

const authenticationMiddleware = async (req, res, next) => {
    const authHeader = req.headers.authorization;
    //validation when header authentication empty
    if (!authHeader || !authHeader.startsWith('Bearer ')) {
        return res.status(401).send({
            status: 'failed',
            msg: 'Not authorized to access this route',
        });
    }

    const token = authHeader.split(' ')[1];

    try {
        const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
        const { id, username } = decoded;
        req.user = { id, username };
        return next();
    } catch (error) {
        //validation when header authentication
        return res.status(401).send({
            status: 'failed',
            msg: 'Not authorized to access this route',
        });
    }
};

module.exports = authenticationMiddleware;
