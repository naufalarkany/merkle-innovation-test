function printLabel() {
    for (let i = 1; i <= 5; i++) {
        let row = '';
        for (let j = 1; j <= 5; j++) {
            row += i * j + ' ';
        }
        console.log(row);
    }
}

// Call the function to print the pattern
printLabel();
