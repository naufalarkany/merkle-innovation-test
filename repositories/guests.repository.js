const { Op } = require('sequelize');
const { Guest: Model } = require('../models');

exports.create = async (newGuest) =>
    new Promise((resolve, reject) => {
        Model.create(newGuest)
            .then((data) => {
                resolve(data);
            })
            .catch(reject);
    });

exports.getAll = () =>
    new Promise((resolve, reject) => {
        Model.findAll()
            .then((data) => {
                resolve(data ? data : null);
            })
            .catch(reject);
    });

exports.getAllForGuests = () =>
    new Promise((resolve, reject) => {
        Model.findAll({
            attributes: ['name', 'notes'],
        })
            .then((data) => {
                resolve(data ? data : null);
            })
            .catch(reject);
    });

exports.getOne = (guestId) =>
    new Promise((resolve, reject) => {
        Model.findOne({
            where: {
                id: guestId,
            },
        })
            .then((data) => {
                resolve(data ? data : null);
            })
            .catch(reject);
    });

exports.delete = async (guestId) =>
    new Promise((resolve, reject) => {
        Model.destroy({
            where: {
                id: guestId,
            },
        })
            .then(resolve)
            .catch(reject);
    });

exports.edit = async (guestId, editedTask) =>
    new Promise((resolve, reject) => {
        Model.update(editedTask, {
            where: {
                id: guestId,
            },
        })
            .then((data) => {
                resolve(data);
            })
            .catch(reject);
    });
