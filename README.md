# print-label

Mencetak tampilan berikut:

1 2 3 4 5

2 4 6 8 10

3 6 9 12 15

4 8 12 16 20

5 10 15 20 25

1. node printLabel.js

# wedding-management

Untuk dokumentasi API klik [disini](https://documenter.getpostman.com/view/14914411/2s9Y5bRgxa) or <https://documenter.getpostman.com/view/14914411/2s9Y5bRgxa>

Untuk jalankan API

Buatlah file .env dan isi value sesuai environment kalian masing masing.

1. npm install
2. npm run db:create
3. npm run db:migrate
4. npm start

Terima kasih!
